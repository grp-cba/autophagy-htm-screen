CellProfiler Pipeline: http://www.cellprofiler.org
Version:1
SVNRevision:11047

LoadImages:[module_num:1|svn_version:\'11031\'|variable_revision_number:11|show_window:False|notes:\x5B\'TODO\x3A add automated background measurements\'\x5D]
    File type to be loaded:individual images
    File selection method:Text-Exact match
    Number of images in each group?:3
    Type the text that the excluded images have in common:Do not use
    Analyze all subfolders within the selected folder?:Some
    Input image file location:Elsewhere...\x7C/g/almfscreen/kenta/ScanR/output
    Check image sets for missing or duplicate files?:Yes
    Group images by metadata?:No
    Exclude certain files?:No
    Specify metadata fields to group by:
    Select subfolders to analyze:Hamasaki_Autoscreen_Plate05_batch1_01,Hamasaki_Autoscreen_Plate_01_batch1_03,Hamasaki_Autoscreen_Plate06_batch1_07,Hamasaki_Autoscreen_Plate_01_batch1_06,Hamasaki_Autoscreen_Plate_01_batch1_05,Hamasaki_Autoscreen_Plate_01_batch1_04,Hamasaki_Autoscreen_Plate06_batch1_04,Hamasaki_Autoscreen_Plate05_batch1_06,Hamasaki_Autoscreen_Plate05_batch1_05,Hamasaki_Autoscreen_Plate05_batch1_04,Hamasaki_Autoscreen_Plate05_batch1_03,Hamasaki_Autoscreen_Plate06_batch1_01,Hamasaki_Autoscreen_Plate06_batch1_02,Hamasaki_Autoscreen_Plate06_batch1_03,Hamasaki_autoscreen_Plate_02_batch1_01,Hamasaki_autoscreen_Plate_02_batch1_02,Hamasaki_autoscreen_Plate_02_batch1_03,Hamasaki_autoscreen_Plate_02_batch1_04,Hamasaki_Autoscreen_Plate06_batch1_05,Hamasaki_Autoscreen_Plate05_batch1_08,Hamasaki_Autoscreen_Plate06_batch1_08,Hamasaki_Autoscreen_Plate_02_batch1_08,Hamasaki_Autoscreen_Plate_02_batch1_05,Hamasaki_Autoscreen_Plate_02_batch1_06,Hamasaki_Autoscreen_Plate_02_batch1_07,Hamasaki_Autoscreen_Plate04_batch1_06,Hamasaki_Autoscreen_Plate04_batch1_07,Hamasaki_Autoscreen_Plate04_batch1_04,Hamasaki_Autoscreen_Plate04_batch1_05,Hamasaki_Autoscreen_Plate04_batch1_03,Hamasaki_Autoscreen_Plate04_batch1_08,Hamasaki_Autoscreen_Plate03_batch1_05,Hamasaki_Autoscreen_Plate03_batch1_07,Hamasaki_Autoscreen_Plate03_batch1_06,Neg9_plates_batch1_03--batchRunner,Hamasaki_Autoscreen_Plate05_batch1_07,Hamasaki_autoscreen_Plate04_batch1_02,Hamasaki_autoscreen_Plate04_batch1_01,Hamasaki_autoscreen_Plate_01_batch1_02,Hamasaki_autoscreen_Plate_01_batch1_01,Hamasaki_Autoscreen_Plate06_batch1_06,Hamasaki_autoscreen_Plate03_batch1_01,Hamasaki_autoscreen_Plate03_batch1_02,Hamasaki_Autoscreen_Plate05_batch1_02
    Image count:3
    Text that these images have in common (case-sensitive):C01.ome.tif
    Position of this image in each group:1
    Extract metadata from where?:Both
    Regular expression that finds metadata in the file name:^(?P<PlateRepl>.*)--(?P<siRNA>.*)--(?P<gene>.*)--W(?P<WellNum>.*)--P(?P<PosNum>.*)--T.*
    Type the regular expression that finds metadata in the subfolder path:(?P<pathBase>.*)\x5B\\\\/\x5D(?P<pathPlate>.*)\x5B\\\\/\x5D(?P<pathWell>.*)\x5B\\\\/\x5D(?P<pathPos>.*)$
    Channel count:1
    Group the movie frames?:No
    Grouping method:Interleaved
    Number of channels per group:3
    Load the input as images or objects?:Images
    Name this loaded image:DNA
    Name this loaded object:Nuclei
    Retain outlines of loaded objects?:No
    Name the outline image:LoadedImageOutlines
    Channel number:1
    Rescale intensities?:Yes
    Text that these images have in common (case-sensitive):C02.ome.tif
    Position of this image in each group:2
    Extract metadata from where?:File name
    Regular expression that finds metadata in the file name:^(?P<dum>.*)--(?P<treatment>.*--.*)--W.*
    Type the regular expression that finds metadata in the subfolder path:.*\x5B\\\\/\x5D(?P<PlateName>.*)$
    Channel count:1
    Group the movie frames?:No
    Grouping method:Interleaved
    Number of channels per group:3
    Load the input as images or objects?:Images
    Name this loaded image:GFP
    Name this loaded object:Nuclei
    Retain outlines of loaded objects?:No
    Name the outline image:LoadedImageOutlines
    Channel number:1
    Rescale intensities?:Yes
    Text that these images have in common (case-sensitive):C03.ome.tif
    Position of this image in each group:3
    Extract metadata from where?:File name
    Regular expression that finds metadata in the file name:^(?P<imageBase>.*)--T.*
    Type the regular expression that finds metadata in the subfolder path:.*\x5B\\\\/\x5D(?P<Date>.*)\x5B\\\\/\x5D(?P<Run>.*)$
    Channel count:1
    Group the movie frames?:No
    Grouping method:Interleaved
    Number of channels per group:3
    Load the input as images or objects?:Images
    Name this loaded image:RFP
    Name this loaded object:Nuclei
    Retain outlines of loaded objects?:No
    Name the outline image:LoadedImageOutlines
    Channel number:1
    Rescale intensities?:Yes

MeasureImageQuality:[module_num:2|svn_version:\'11045\'|variable_revision_number:4|show_window:False|notes:\x5B\'i) \', \'\', \'Q) how did we determined window size for blur measurements 20? \'\x5D]
    Calculate metrics for which images?:Select...
    Image count:1
    Scale count:1
    Threshold count:1
    Select the images to measure:DNA,GFP,RFP
    Include the image rescaling value?:Yes
    Calculate blur metrics?:Yes
    Window size for blur measurements:20
    Calculate saturation metrics?:No
    Calculate intensity metrics?:Yes
    Calculate thresholds?:No
    Use all thresholding methods?:Yes
    Select a thresholding method:Otsu Global
    Typical fraction of the image covered by objects:0.1
    Two-class or three-class thresholding?:Two classes
    Minimize the weighted variance or the entropy?:Weighted variance
    Assign pixels in the middle intensity class to the foreground or the background?:Foreground

Smooth:[module_num:3|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\'i) modify Hoechst staining for better detection of Nuclei\'\x5D]
    Select the input image:DNA
    Name the output image:DNA-Median
    Select smoothing method:Median Filter
    Calculate artifact diameter automatically?:No
    Typical artifact diameter, in  pixels:5
    Edge intensity difference:0.1

IdentifyPrimaryObjects:[module_num:4|svn_version:\'11047\'|variable_revision_number:8|show_window:False|notes:\x5B\'i) finding Nuclei based on smoothed Hoechst staining\', \'\', \'Find Nuclei\'\x5D]
    Select the input image:DNA-Median
    Name the primary objects to be identified:NucleiTmp
    Typical diameter of objects, in pixel units (Min,Max):30,10000
    Discard objects outside the diameter range?:Yes
    Try to merge too small objects with nearby larger objects?:No
    Discard objects touching the border of the image?:No
    Select the thresholding method:Manual
    Threshold correction factor:1.0
    Lower and upper bounds on threshold:0.000000,1.000000
    Approximate fraction of image covered by objects?:0.01
    Method to distinguish clumped objects:Shape
    Method to draw dividing lines between clumped objects:Shape
    Size of smoothing filter:10
    Suppress local maxima that are closer than this minimum allowed distance:7
    Speed up by using lower-resolution image to find local maxima?:Yes
    Name the outline image:PrimaryOutlines
    Fill holes in identified objects?:Yes
    Automatically calculate size of smoothing filter?:Yes
    Automatically calculate minimum allowed distance between local maxima?:Yes
    Manual threshold:0.008
    Select binary image:None
    Retain outlines of the identified objects?:No
    Automatically calculate the threshold using the Otsu method?:Yes
    Enter Laplacian of Gaussian threshold:0.5
    Two-class or three-class thresholding?:Two classes
    Minimize the weighted variance or the entropy?:Weighted variance
    Assign pixels in the middle intensity class to the foreground or the background?:Foreground
    Automatically calculate the size of objects for the Laplacian of Gaussian filter?:Yes
    Enter LoG filter diameter:5
    Handling of objects if excessive number of objects identified:Continue
    Maximum number of objects:500
    Select the measurement to threshold with:None

ImageMath:[module_num:5|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\'i) substract GFP back ground (GFPbg)\', \'\', \'to do) Check number to be subtracted for each plate. Use ImageJ and Excel for this.\', \'\'\x5D]
    Operation:None
    Raise the power of the result by:1
    Multiply the result by:1
    Add to result:-0.003051804
    Set values less than 0 equal to 0?:No
    Set values greater than 1 equal to 1?:Yes
    Ignore the image masks?:No
    Name the output image:GFPbg
    Image or measurement?:Image
    Select the first image:GFP
    Multiply the first image by:1
    Measurement:
    Image or measurement?:Image
    Select the second image:
    Multiply the second image by:1
    Measurement:

ImageMath:[module_num:6|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\'i) substract RFP back ground (RFPbg)\', \'\', \'to do) Check number to be subtracted for each plate. Use ImageJ and Excel for this; \'\x5D]
    Operation:None
    Raise the power of the result by:1
    Multiply the result by:1
    Add to result:-0.003051804
    Set values less than 0 equal to 0?:No
    Set values greater than 1 equal to 1?:Yes
    Ignore the image masks?:No
    Name the output image:RFPbg
    Image or measurement?:Image
    Select the first image:RFP
    Multiply the first image by:1
    Measurement:
    Image or measurement?:Image
    Select the second image:
    Multiply the second image by:1
    Measurement:

Smooth:[module_num:7|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select the input image:GFP
    Name the output image:GFPgauss
    Select smoothing method:Gaussian Filter
    Calculate artifact diameter automatically?:No
    Typical artifact diameter, in  pixels:7
    Edge intensity difference:0.1

Morph:[module_num:8|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\'play with "scale";\', \'scale/2 is the distance in which the local background (minimum intensity) for each pixel is looked for.\', \'GFPerode contains this BG value\', \'\', \'\'\x5D]
    Select the input image:GFPgauss
    Name the output image:GFPerode
    Select the operation to perform:erode
    Number of times to repeat operation:Once
    Repetition number:2
    Scale:20

ImageMath:[module_num:9|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\'Here, the local BG is subtracted from the smoothed raw data\'\x5D]
    Operation:Subtract
    Raise the power of the result by:1
    Multiply the result by:1
    Add to result:0
    Set values less than 0 equal to 0?:Yes
    Set values greater than 1 equal to 1?:Yes
    Ignore the image masks?:No
    Name the output image:GFPlocalBG
    Image or measurement?:Image
    Select the first image:GFPgauss
    Multiply the first image by:1.0
    Measurement:
    Image or measurement?:Image
    Select the second image:GFPerode
    Multiply the second image by:1
    Measurement:

IdentifySecondaryObjects:[module_num:10|svn_version:\'11025\'|variable_revision_number:7|show_window:False|notes:\x5B\'i) find cells based on smoothed GFP signal (GFPbgSmooth)\', \'\', \'To do) play with the "manual threshold" (this is now independent of the global bg subtraction in GFPbg (ImageMath above))\', \'\'\x5D]
    Select the input objects:NucleiTmp
    Name the objects to be identified:CellsTmp
    Select the method to identify the secondary objects:Watershed - Image
    Select the input image:GFPlocalBG
    Select the thresholding method:Manual
    Threshold correction factor:1
    Lower and upper bounds on threshold:0.000000,1.000000
    Approximate fraction of image covered by objects?:0.01
    Number of pixels by which to expand the primary objects:10
    Regularization factor:0.01
    Name the outline image:CellsOutlines
    Manual threshold:0.00015
    Select binary image:None
    Retain outlines of the identified secondary objects?:No
    Two-class or three-class thresholding?:Two classes
    Minimize the weighted variance or the entropy?:Weighted variance
    Assign pixels in the middle intensity class to the foreground or the background?:Foreground
    Discard secondary objects that touch the edge of the image?:Yes
    Discard the associated primary objects?:Yes
    Name the new primary objects:Nuclei
    Retain outlines of the new primary objects?:Yes
    Name the new primary object outlines:NucleiOutlines
    Select the measurement to threshold with:None
    Fill holes in identified objects?:Yes

ExpandOrShrinkObjects:[module_num:11|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select the input objects:CellsTmp
    Name the output objects:Cells
    Select the operation:Expand objects by a specified number of pixels
    Number of pixels by which to expand or shrink:1
    Fill holes in objects so that all objects shrink to a single point?:No
    Retain the outlines of the identified objects for use later in the pipeline (for example, in SaveImages)?:Yes
    Name the outline image:CellOutlines

OverlayOutlines:[module_num:12|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Display outlines on a blank image?:No
    Select image on which to display outlines:GFPbg
    Name the output image:OrigOverlay
    Select outline display mode:Color
    Select method to determine brightness of outlines:Max of image
    Width of outlines:2
    Select outlines to display:CellOutlines
    Select outline color:Blue

MeasureObjectIntensity:[module_num:13|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\'\'\x5D]
    Hidden:3
    Select an image to measure:GFPbg
    Select an image to measure:GFP
    Select an image to measure:RFP
    Select objects to measure:Cells

DisplayDataOnImage:[module_num:14|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\'i) To just show GFP(RFP) total signal in each cells.\', \'I think it may be safe that this processure is done based on RFP.\', \'\', \'Only for visualisation; here are no computations done.\', \'Check here for a threshold to filter cells expressing to little GFP \', \'\'\x5D]
    Display object or image measurements?:Object
    Select the input objects:Cells
    Measurement to display:Intensity_IntegratedIntensity_GFPbg
    Select the image on which to display the measurements:GFPbg
    Text color:red
    Name the output image that has the measurements displayed:DisplayImage
    Font size (points):10
    Number of decimals:2
    Image elements to save:Image

DisplayDataOnImage:[module_num:15|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\'Maximum intensity in the GFP image (NOT GFPbg!) to check for saturation\', \'\'\x5D]
    Display object or image measurements?:Object
    Select the input objects:Cells
    Measurement to display:Intensity_MaxIntensity_GFP
    Select the image on which to display the measurements:GFP
    Text color:red
    Name the output image that has the measurements displayed:DisplayImage
    Font size (points):10
    Number of decimals:7
    Image elements to save:Image

DisplayDataOnImage:[module_num:16|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Display object or image measurements?:Object
    Select the input objects:Cells
    Measurement to display:Intensity_MaxIntensity_RFP
    Select the image on which to display the measurements:RFP
    Text color:red
    Name the output image that has the measurements displayed:DisplayImage
    Font size (points):10
    Number of decimals:4
    Image elements to save:Image

FilterObjects:[module_num:17|svn_version:\'11025\'|variable_revision_number:5|show_window:False|notes:\x5B\'i) To remove cells which have extremely low expression of tfLC3.\', \'To do) Enter threshold to filter cells expressing to little GFP (RFP)\', \'ii) To remove cells saturated in the GFP or RFP signal\'\x5D]
    Name the output objects:GfpCells
    Select the object to filter:Cells
    Filter using classifier rules or measurements?:Measurements
    Select the filtering method:Limits
    Select the objects that contain the filtered objects:None
    Retain outlines of the identified objects?:Yes
    Name the outline image:GfpCellsOutlines
    Rules file location:Default Input Folder\x7CNone
    Rules file name:rules.txt
    Measurement count:3
    Additional object count:1
    Select the measurement to filter by:Intensity_IntegratedIntensity_GFPbg
    Filter using a minimum measurement value?:Yes
    Minimum value:10.0
    Filter using a maximum measurement value?:No
    Maximum value:1
    Select the measurement to filter by:Intensity_MaxIntensity_GFP
    Filter using a minimum measurement value?:No
    Minimum value:0
    Filter using a maximum measurement value?:Yes
    Maximum value:0.06247
    Select the measurement to filter by:Intensity_MaxIntensity_RFP
    Filter using a minimum measurement value?:No
    Minimum value:0
    Filter using a maximum measurement value?:Yes
    Maximum value:0.06247
    Select additional object to relabel:Nuclei
    Name the relabeled objects:GfpNuclei
    Retain outlines of relabeled objects?:Yes
    Name the outline image:GfpNucleiOutlines

ExpandOrShrinkObjects:[module_num:18|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\'Prepare automated BG measurements\'\x5D]
    Select the input objects:Cells
    Name the output objects:CellsExpanded
    Select the operation:Expand objects by a specified number of pixels
    Number of pixels by which to expand or shrink:20
    Fill holes in objects so that all objects shrink to a single point?:No
    Retain the outlines of the identified objects for use later in the pipeline (for example, in SaveImages)?:No
    Name the outline image:ShrunkenNucleiOutlines

ConvertObjectsToImage:[module_num:19|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select the input objects:CellsExpanded
    Name the output image:CellsExpanded
    Select the color type:Binary (black & white)
    Select the colormap:Default

Crop:[module_num:20|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:CellsExpanded
    Name the output image:CellsExpandedCrop
    Select the cropping shape:Rectangle
    Select the cropping method:Coordinates
    Apply which cycle\'s cropping pattern?:Every
    Left and right rectangle positions:150,-150
    Top and bottom rectangle positions:150,-150
    Coordinates of ellipse center:500,500
    Ellipse radius, X direction:400
    Ellipse radius, Y direction:200
    Use Plate Fix?:No
    Remove empty rows and columns?:All
    Select the masking image:None
    Select the image with a cropping mask:None
    Select the objects:None

Crop:[module_num:21|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:GFPbg
    Name the output image:GFPbgCrop
    Select the cropping shape:Rectangle
    Select the cropping method:Coordinates
    Apply which cycle\'s cropping pattern?:Every
    Left and right rectangle positions:150,-150
    Top and bottom rectangle positions:150,-150
    Coordinates of ellipse center:500,500
    Ellipse radius, X direction:400
    Ellipse radius, Y direction:200
    Use Plate Fix?:No
    Remove empty rows and columns?:All
    Select the masking image:None
    Select the image with a cropping mask:None
    Select the objects:None

Crop:[module_num:22|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:RFPbg
    Name the output image:RFPbgCrop
    Select the cropping shape:Rectangle
    Select the cropping method:Coordinates
    Apply which cycle\'s cropping pattern?:Every
    Left and right rectangle positions:150,-150
    Top and bottom rectangle positions:150,-150
    Coordinates of ellipse center:500,500
    Ellipse radius, X direction:400
    Ellipse radius, Y direction:200
    Use Plate Fix?:No
    Remove empty rows and columns?:All
    Select the masking image:None
    Select the image with a cropping mask:None
    Select the objects:None

MaskImage:[module_num:23|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\'automated bg mesurement?\'\x5D]
    Select the input image:GFPbgCrop
    Name the output image:GFPbgCropMask
    Use objects or an image as a mask?:Image
    Select object for mask:CellsExpanded
    Select image for mask:CellsExpandedCrop
    Invert the mask?:Yes

MaskImage:[module_num:24|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\'automated BG measurement\'\x5D]
    Select the input image:RFPbgCrop
    Name the output image:RFPbgCropMask
    Use objects or an image as a mask?:Image
    Select object for mask:CellsExpanded
    Select image for mask:CellsExpandedCrop
    Invert the mask?:Yes

MeasureImageIntensity:[module_num:25|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\'automated BG measurement\'\x5D]
    Select the image to measure:RFPbgCropMask
    Measure the intensity only from areas enclosed by objects?:No
    Select the input objects:None
    Select the image to measure:GFPbgCropMask
    Measure the intensity only from areas enclosed by objects?:No
    Select the input objects:None

EnhanceOrSuppressFeatures:[module_num:26|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Select the input image:GFP
    Name the output image:GFPtophat
    Select the operation:Enhance
    Feature size:9
    Feature type:Speckles
    Range of hole sizes:1,10
    Smoothing scale:2.0
    Shear angle:0
    Decay:0.95

Smooth:[module_num:27|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select the input image:GFPtophat
    Name the output image:GFPgb
    Select smoothing method:Gaussian Filter
    Calculate artifact diameter automatically?:No
    Typical artifact diameter, in  pixels:7
    Edge intensity difference:0.1

ImageMath:[module_num:28|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Operation:Subtract
    Raise the power of the result by:1
    Multiply the result by:1
    Add to result:0.0
    Set values less than 0 equal to 0?:Yes
    Set values greater than 1 equal to 1?:No
    Ignore the image masks?:No
    Name the output image:GFPgd
    Image or measurement?:Image
    Select the first image:GFPtophat
    Multiply the first image by:1
    Measurement:
    Image or measurement?:Image
    Select the second image:GFPgb
    Multiply the second image by:1
    Measurement:

IdentifyTertiaryObjects:[module_num:29|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select the larger identified objects:GfpCells
    Select the smaller identified objects:GfpNuclei
    Name the tertiary objects to be identified:GfpCytoplasm
    Name the outline image:CytoplasmOutlines
    Retain outlines of the tertiary objects?:No

Crop:[module_num:30|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\'Can we remove this module??\', \'\', \'Q) I may have changed something wrong. Could you check?\', \'\', \'The information where GfpCells are, is "overlayed" onto the GFPtophat image; \', \'this will be used in the next module to segment the GFPdots in all GfpCells.\', \'\'\x5D]
    Select the input image:GFPtophat
    Name the output image:GFPtophatGfpCells
    Select the cropping shape:Objects
    Select the cropping method:Coordinates
    Apply which cycle\'s cropping pattern?:Every
    Left and right rectangle positions:0,end
    Top and bottom rectangle positions:0,end
    Coordinates of ellipse center:500,500
    Ellipse radius, X direction:400
    Ellipse radius, Y direction:200
    Use Plate Fix?:No
    Remove empty rows and columns?:No
    Select the masking image:None
    Select the image with a cropping mask:None
    Select the objects:GfpCytoplasm

Crop:[module_num:31|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:GFPgd
    Name the output image:GFPgdGfpCyto
    Select the cropping shape:Objects
    Select the cropping method:Coordinates
    Apply which cycle\'s cropping pattern?:Every
    Left and right rectangle positions:0,end
    Top and bottom rectangle positions:0,end
    Coordinates of ellipse center:500,500
    Ellipse radius, X direction:400
    Ellipse radius, Y direction:200
    Use Plate Fix?:No
    Remove empty rows and columns?:No
    Select the masking image:None
    Select the image with a cropping mask:None
    Select the objects:GfpCytoplasm

IdentifyPrimaryObjects:[module_num:32|svn_version:\'11047\'|variable_revision_number:8|show_window:True|notes:\x5B\'Here the GfpDots are identified in the Cytoplasm\', \'\', \'Typical diameter of objects(Min/Max) \x3A default 1/400 -> 2/400(critical!)\', \'Threshold correction factor \x3A default 3(bad detection) -> 2(critical and good more dots) -> 2.5 (almost same as 2)\', \'Lower and upper bounds on threshold \x3A default 0.0/1.0\', \'Enter Lo G filter diameter \x3A default 3.0\'\x5D]
    Select the input image:GFPgdGfpCyto
    Name the primary objects to be identified:GfpCytoDots
    Typical diameter of objects, in pixel units (Min,Max):2,100
    Discard objects outside the diameter range?:Yes
    Try to merge too small objects with nearby larger objects?:No
    Discard objects touching the border of the image?:No
    Select the thresholding method:Otsu PerObject
    Threshold correction factor:3
    Lower and upper bounds on threshold:0.0,1.0
    Approximate fraction of image covered by objects?:0.1
    Method to distinguish clumped objects:Laplacian of Gaussian
    Method to draw dividing lines between clumped objects:Intensity
    Size of smoothing filter:0
    Suppress local maxima that are closer than this minimum allowed distance:3
    Speed up by using lower-resolution image to find local maxima?:No
    Name the outline image:GfpCytoDotsOutlines
    Fill holes in identified objects?:Yes
    Automatically calculate size of smoothing filter?:No
    Automatically calculate minimum allowed distance between local maxima?:No
    Manual threshold:0.0
    Select binary image:None
    Retain outlines of the identified objects?:Yes
    Automatically calculate the threshold using the Otsu method?:Yes
    Enter Laplacian of Gaussian threshold:0.5
    Two-class or three-class thresholding?:Two classes
    Minimize the weighted variance or the entropy?:Weighted variance
    Assign pixels in the middle intensity class to the foreground or the background?:Background
    Automatically calculate the size of objects for the Laplacian of Gaussian filter?:No
    Enter LoG filter diameter:5
    Handling of objects if excessive number of objects identified:Continue
    Maximum number of objects:500
    Select the measurement to threshold with:None

Crop:[module_num:33|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:GFPgd
    Name the output image:GFPgdGfpNuclei
    Select the cropping shape:Objects
    Select the cropping method:Coordinates
    Apply which cycle\'s cropping pattern?:Every
    Left and right rectangle positions:0,end
    Top and bottom rectangle positions:0,end
    Coordinates of ellipse center:500,500
    Ellipse radius, X direction:400
    Ellipse radius, Y direction:200
    Use Plate Fix?:No
    Remove empty rows and columns?:No
    Select the masking image:None
    Select the image with a cropping mask:None
    Select the objects:Nuclei

IdentifyPrimaryObjects:[module_num:34|svn_version:\'11047\'|variable_revision_number:8|show_window:False|notes:\x5B\'Find GFP dots in the Nucleus\'\x5D]
    Select the input image:GFPgdGfpNuclei
    Name the primary objects to be identified:GfpNucleusDots
    Typical diameter of objects, in pixel units (Min,Max):2,100
    Discard objects outside the diameter range?:Yes
    Try to merge too small objects with nearby larger objects?:No
    Discard objects touching the border of the image?:No
    Select the thresholding method:RobustBackground PerObject
    Threshold correction factor:3
    Lower and upper bounds on threshold:0.0,1.0
    Approximate fraction of image covered by objects?:0.1
    Method to distinguish clumped objects:Laplacian of Gaussian
    Method to draw dividing lines between clumped objects:Intensity
    Size of smoothing filter:0
    Suppress local maxima that are closer than this minimum allowed distance:3
    Speed up by using lower-resolution image to find local maxima?:No
    Name the outline image:GfpNucleusDotsOutlines
    Fill holes in identified objects?:Yes
    Automatically calculate size of smoothing filter?:No
    Automatically calculate minimum allowed distance between local maxima?:No
    Manual threshold:0.0
    Select binary image:None
    Retain outlines of the identified objects?:Yes
    Automatically calculate the threshold using the Otsu method?:Yes
    Enter Laplacian of Gaussian threshold:0.5
    Two-class or three-class thresholding?:Two classes
    Minimize the weighted variance or the entropy?:Weighted variance
    Assign pixels in the middle intensity class to the foreground or the background?:Background
    Automatically calculate the size of objects for the Laplacian of Gaussian filter?:No
    Enter LoG filter diameter:2
    Handling of objects if excessive number of objects identified:Continue
    Maximum number of objects:500
    Select the measurement to threshold with:None

ExpandOrShrinkObjects:[module_num:35|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\'by shrinking the dots to points one can simply count them...\'\x5D]
    Select the input objects:GfpCytoDots
    Name the output objects:GfpCytoDotPoints
    Select the operation:Shrink objects to a point
    Number of pixels by which to expand or shrink:1
    Fill holes in objects so that all objects shrink to a single point?:Yes
    Retain the outlines of the identified objects for use later in the pipeline (for example, in SaveImages)?:No
    Name the outline image:ShrunkenNucleiOutlines

ExpandOrShrinkObjects:[module_num:36|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select the input objects:GfpNucleusDots
    Name the output objects:GfpNucleusDotPoints
    Select the operation:Shrink objects to a point
    Number of pixels by which to expand or shrink:1
    Fill holes in objects so that all objects shrink to a single point?:Yes
    Retain the outlines of the identified objects for use later in the pipeline (for example, in SaveImages)?:No
    Name the outline image:ShrunkenNucleiOutlines

ConvertObjectsToImage:[module_num:37|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select the input objects:GfpCytoDotPoints
    Name the output image:GfpCytoDotPoints
    Select the color type:Binary (black & white)
    Select the colormap:Default

ConvertObjectsToImage:[module_num:38|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select the input objects:GfpNucleusDotPoints
    Name the output image:GfpNucleusDotPoints
    Select the color type:Binary (black & white)
    Select the colormap:Default

ImageMath:[module_num:39|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B"This image counts as many 1\'s as there are dots, so one can simply measure its integrated intensity to count the dots"\x5D]
    Operation:Add
    Raise the power of the result by:1
    Multiply the result by:1
    Add to result:0
    Set values less than 0 equal to 0?:Yes
    Set values greater than 1 equal to 1?:Yes
    Ignore the image masks?:No
    Name the output image:GfpDotPointsBW
    Image or measurement?:Image
    Select the first image:GfpCytoDotPoints
    Multiply the first image by:1.0
    Measurement:
    Image or measurement?:Image
    Select the second image:GfpNucleusDotPoints
    Multiply the second image by:1
    Measurement:

ConvertObjectsToImage:[module_num:40|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select the input objects:GfpCytoDots
    Name the output image:GFPdotsBW
    Select the color type:Binary (black & white)
    Select the colormap:Default

ConvertObjectsToImage:[module_num:41|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select the input objects:Nuclei
    Name the output image:NucleiMask
    Select the color type:Binary (black & white)
    Select the colormap:Default

ImageMath:[module_num:42|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\'Dot Distance Measurements\'\x5D]
    Operation:Invert
    Raise the power of the result by:1
    Multiply the result by:1
    Add to result:0
    Set values less than 0 equal to 0?:Yes
    Set values greater than 1 equal to 1?:Yes
    Ignore the image masks?:No
    Name the output image:NucleiMaskInv
    Image or measurement?:Image
    Select the first image:NucleiMask
    Multiply the first image by:1
    Measurement:
    Image or measurement?:Image
    Select the second image:
    Multiply the second image by:1
    Measurement:

ApplyThreshold:[module_num:43|svn_version:\'6746\'|variable_revision_number:5|show_window:False|notes:\x5B\'Dot Distance Measurements\', \'just a dummy step to make the image type binary (otherwise the next module reports a warning when running on the cluster)\', \'\'\x5D]
    Select the input image:NucleiMaskInv
    Name the output image:NucleiMaskInvBin
    Select the output image type:Binary (black and white)
    Set pixels below or above the threshold to zero?:Below threshold
    Subtract the threshold value from the remaining pixel intensities?:No
    Number of pixels by which to expand the thresholding around those excluded bright pixels:0.0
    Select the thresholding method:Manual
    Manual threshold:0.5
    Lower and upper bounds on threshold:0.000000,1.000000
    Threshold correction factor:1
    Approximate fraction of image covered by objects?:0.01
    Select the input objects:None
    Two-class or three-class thresholding?:Two classes
    Minimize the weighted variance or the entropy?:Weighted variance
    Assign pixels in the middle intensity class to the foreground or the background?:Foreground
    Select the measurement to threshold with:None

Morph:[module_num:44|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\'Dot Distance Measurements\'\x5D]
    Select the input image:NucleiMaskInvBin
    Name the output image:NucleiDist
    Select the operation to perform:distance
    Number of times to repeat operation:Once
    Repetition number:2
    Scale:3

ImageMath:[module_num:45|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\'Dot Distance Measurements\'\x5D]
    Operation:Multiply
    Raise the power of the result by:1
    Multiply the result by:1
    Add to result:0
    Set values less than 0 equal to 0?:Yes
    Set values greater than 1 equal to 1?:No
    Ignore the image masks?:No
    Name the output image:NucleiDistDots
    Image or measurement?:Image
    Select the first image:NucleiDist
    Multiply the first image by:1
    Measurement:
    Image or measurement?:Image
    Select the second image:GFPdotsBW
    Multiply the second image by:1
    Measurement:

ImageMath:[module_num:46|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\'Dot Distance Measurements\', \'set all pixels to zero that are not segmented as GfpDots\'\x5D]
    Operation:Multiply
    Raise the power of the result by:1
    Multiply the result by:1
    Add to result:0
    Set values less than 0 equal to 0?:Yes
    Set values greater than 1 equal to 1?:No
    Ignore the image masks?:No
    Name the output image:GFPtophatDots
    Image or measurement?:Image
    Select the first image:GFPtophat
    Multiply the first image by:1
    Measurement:
    Image or measurement?:Image
    Select the second image:GFPdotsBW
    Multiply the second image by:1
    Measurement:

MeasureObjectIntensity:[module_num:47|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Hidden:5
    Select an image to measure:GFPbg
    Select an image to measure:RFPbg
    Select an image to measure:GFPtophatDots
    Select an image to measure:GFPdotsBW
    Select an image to measure:GfpDotPointsBW
    Select objects to measure:GfpCells

FilterObjects:[module_num:48|svn_version:\'11025\'|variable_revision_number:5|show_window:False|notes:\x5B\x5D]
    Name the output objects:GfpCellsWithDots
    Select the object to filter:GfpCells
    Filter using classifier rules or measurements?:Measurements
    Select the filtering method:Limits
    Select the objects that contain the filtered objects:None
    Retain outlines of the identified objects?:No
    Name the outline image:FilteredObjects
    Rules file location:Default Input Folder\x7CNone
    Rules file name:rules.txt
    Measurement count:1
    Additional object count:0
    Select the measurement to filter by:Intensity_IntegratedIntensity_GFPdotsBW
    Filter using a minimum measurement value?:Yes
    Minimum value:1.0
    Filter using a maximum measurement value?:No
    Maximum value:1

MeasureObjectIntensity:[module_num:49|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Hidden:2
    Select an image to measure:GFPdotsBW
    Select an image to measure:NucleiDistDots
    Select objects to measure:GfpCellsWithDots

MeasureObjectIntensity:[module_num:50|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Hidden:1
    Select an image to measure:NucleiDist
    Select objects to measure:GfpCytoDots

MeasureObjectSizeShape:[module_num:51|svn_version:\'1\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select objects to measure:GfpCells
    Calculate the Zernike features?:No

CalculateMath:[module_num:52|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\'Result\', \'GFP dot distance\'\x5D]
    Name the output measurement:MeanDist
    Operation:Divide
    Select the numerator measurement type:Object
    Select the numerator objects:GfpCellsWithDots
    Select the numerator measurement:Intensity_IntegratedIntensity_NucleiDistDots
    Multiply the above operand by:1
    Raise the power of above operand by:1
    Select the denominator measurement type:Object
    Select the denominator objects:GfpCellsWithDots
    Select the denominator measurement:Intensity_IntegratedIntensity_GFPdotsBW
    Multiply the above operand by:1
    Raise the power of above operand by:1
    Take log10 of result?:No
    Multiply the result by:1
    Raise the power of result by:1

CalculateMath:[module_num:53|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\'Result\', \'Ratio GFP total RFP total\'\x5D]
    Name the output measurement:RatioGfpTotRfpTot
    Operation:Divide
    Select the numerator measurement type:Object
    Select the numerator objects:GfpCells
    Select the numerator measurement:Intensity_IntegratedIntensity_GFPbg
    Multiply the above operand by:1
    Raise the power of above operand by:1
    Select the denominator measurement type:Object
    Select the denominator objects:GfpCells
    Select the denominator measurement:Intensity_IntegratedIntensity_RFPbg
    Multiply the above operand by:1
    Raise the power of above operand by:1
    Take log10 of result?:No
    Multiply the result by:1
    Raise the power of result by:1

CalculateMath:[module_num:54|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\'Result\', \'GFP dot / GFP total intensity\'\x5D]
    Name the output measurement:RatioGfpDotsGfpTot
    Operation:Divide
    Select the numerator measurement type:Object
    Select the numerator objects:GfpCells
    Select the numerator measurement:Intensity_IntegratedIntensity_GFPtophatDots
    Multiply the above operand by:1
    Raise the power of above operand by:1
    Select the denominator measurement type:Object
    Select the denominator objects:GfpCells
    Select the denominator measurement:Intensity_IntegratedIntensity_GFPbg
    Multiply the above operand by:1
    Raise the power of above operand by:1
    Take log10 of result?:No
    Multiply the result by:1
    Raise the power of result by:1

CalculateMath:[module_num:55|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Name the output measurement:GfpDotsCount
    Operation:None
    Select the numerator measurement type:Object
    Select the numerator objects:GfpCells
    Select the numerator measurement:Intensity_IntegratedIntensity_GfpDotPointsBW
    Multiply the above operand by:1
    Raise the power of above operand by:1
    Select the second operand measurement type:Object
    Select the second operand objects:GfpCells
    Select the second operand measurement:Math_GfpNucDotsCount
    Multiply the above operand by:1
    Raise the power of above operand by:1
    Take log10 of result?:No
    Multiply the result by:1
    Raise the power of result by:1

RescaleIntensity:[module_num:56|svn_version:\'6746\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:GFP
    Name the output image:GFP-RS
    Select rescaling method:Stretch each image to use the full intensity range
    How do you want to calculate the minimum intensity?:Custom
    How do you want to calculate the maximum intensity?:Custom
    Enter the lower limit for the intensity range for the input image:0
    Enter the upper limit for the intensity range for the input image:1
    Enter the intensity range for the input image:0.000000,1.000000
    Enter the desired intensity range for the final, rescaled image:0.000000,1.000000
    Select method for rescaling pixels below the lower limit:Mask pixels
    Enter custom value for pixels below lower limit:0
    Select method for rescaling pixels above the upper limit:Mask pixels
    Enter custom value for pixels below upper limit:0
    Select image to match in maximum intensity:None
    Enter the divisor:1
    Select the measurement to use as a divisor:None

OverlayOutlines:[module_num:57|svn_version:\'11025\'|variable_revision_number:2|show_window:True|notes:\x5B\'only for visualisation; nothing is measured here\', \'\'\x5D]
    Display outlines on a blank image?:No
    Select image on which to display outlines:GFP-RS
    Name the output image:GFPOverlay
    Select outline display mode:Color
    Select method to determine brightness of outlines:Max of image
    Width of outlines:0.5
    Select outlines to display:GfpNucleiOutlines
    Select outline color:Blue
    Select outlines to display:GfpCellsOutlines
    Select outline color:Blue
    Select outlines to display:GfpCytoDotsOutlines
    Select outline color:Green
    Select outlines to display:GfpNucleusDotsOutlines
    Select outline color:Yellow

Tile:[module_num:58|svn_version:\'9034\'|variable_revision_number:1|show_window:True|notes:\x5B\x5D]
    Select an input image:GFP-RS
    Name the output image:TiledImage
    Tile within cycles or across cycles?:Within cycles
    Number of rows in final tiled image:8
    Number of columns in final tiled image:12
    Begin tiling in which corner of the final image?:top left
    Begin tiling across a row, or down a column?:row
    Tile in meander mode?:No
    Automatically calculate number of rows?:Yes
    Automatically calculate number of columns?:Yes
    Select an additional image to tile:GFPOverlay

SaveImages:[module_num:59|svn_version:\'11042\'|variable_revision_number:7|show_window:False|notes:\x5B\x5D]
    Select the type of image to save:Image
    Select the image to save:TiledImage
    Select the objects to save:None
    Select the module display window to save:None
    Select method for constructing file names:Single name
    Select image name for file prefix:GFP
    Enter single file name:\\g<imageBase>--GFPOverlay.png
    Do you want to add a suffix to the image file name?:No
    Text to append to the image name:
    Select file format to use:png
    Output file location:Elsewhere...\x7C\\g<pathBase>--cp/\\g<pathPlate>--cp
    Image bit depth:8
    Overwrite existing files without warning?:Yes
    Select how often to save:Every cycle
    Rescale the images? :Yes
    Save as grayscale or color image?:Grayscale
    Select colormap:gray
    Store file and path information to the saved image?:Yes
    Create subfolders in the output folder?:No

ExportToSpreadsheet:[module_num:60|svn_version:\'11042\'|variable_revision_number:7|show_window:False|notes:\x5B\x5D]
    Select or enter the column delimiter:Comma (",")
    Prepend the output file name to the data file names?:No
    Add image metadata columns to your object data file?:Yes
    Limit output to a size that is allowed in Excel?:No
    Select the columns of measurements to export?:Yes
    Calculate the per-image mean values for object measurements?:Yes
    Calculate the per-image median values for object measurements?:No
    Calculate the per-image standard deviation values for object measurements?:No
    Output file location:Elsewhere...\x7C\\g<pathBase>--cp/\\g<pathPlate>--cp/W\\g<WellNum>--P\\g<PosNum>/
    Create a GenePattern GCT file?:No
    Select source of sample row name:Metadata
    Select the image to use as the identifier:None
    Select the metadata to use as the identifier:None
    Export all measurements, using default file names?:No
    Press button to select measurements to export:GfpCellsWithDots\x7CMath_MeanDist,Image\x7CCount_GfpCells,Image\x7CCount_Cells,Image\x7CImageQuality_MinIntensity_GFP,Image\x7CImageQuality_MinIntensity_RFP,Image\x7CImageQuality_MinIntensity_DNA,Image\x7CImageQuality_PowerLogLogSlope_DNA,Image\x7CFileName_GFP,Image\x7CFileName_RFP,Image\x7CFileName_DNA,Image\x7CFileName_TiledImage,Image\x7CIntensity_MedianIntensity_RFPbgCropMask,Image\x7CIntensity_MedianIntensity_GFPbgCropMask,Image\x7CPathName_GFP,Image\x7CPathName_RFP,Image\x7CPathName_DNA,Image\x7CPathName_TiledImage,Image\x7CMetadata_PosNum,Image\x7CMetadata_PlateRepl,Image\x7CMetadata_pathWell,Image\x7CMetadata_WellNum,Image\x7CMetadata_pathPlate,Image\x7CMetadata_treatment,Image\x7CMetadata_pathBase,Image\x7CMetadata_imageBase,Image\x7CMetadata_pathPos,Image\x7CMetadata_gene,Image\x7CMetadata_siRNA,GfpCells\x7CNumber_Object_Number,GfpCells\x7CIntensity_MeanIntensity_RFPbg,GfpCells\x7CIntensity_MeanIntensity_GFPbg,GfpCells\x7CIntensity_IntegratedIntensity_RFPbg,GfpCells\x7CIntensity_IntegratedIntensity_GFPbg,GfpCells\x7CAreaShape_Area,GfpCells\x7CMath_RatioGfpDotsGfpTot,GfpCells\x7CMath_GfpDotsCount,GfpCells\x7CMath_RatioGfpTotRfpTot
    Data to export:Image
    Combine these object measurements with those of the previous object?:No
    File name:image.csv
    Use the object name for the file name?:No

CreateBatchFiles:[module_num:61|svn_version:\'11025\'|variable_revision_number:4|show_window:False|notes:\x5B\x5D]
    Store batch files in default output folder?:No
    Output folder path:/g/almfscreen/kenta/analysis
    Are the cluster computers running Windows?:No
    Hidden\x3A in batch mode:No
    Hidden\x3A default input folder at time of save:/home/tischer
    Hidden\x3A SVN revision number:0
    Local root path:/home/tischer
    Cluster root path:/home/tischer
