# Autophagy HTM screen

## Description

Automated quantification of autophagy in fluorescence microscopy images in a high-throughput microscopy (HTM) assay.

The autophagic activity was monitored using a mRFP-GFP tandem fluorescent-tagged LC3 (tfLC3).
We measured the number and total intensity of GFP LC3 puncta per cell.

## Collaborators

- hamasaki@fbs.osaka-u.ac.jp
- kenchanhohoho@gmail.com
- bneumann@embl.de
- sabine.reither@fmi.ch
- christian.tischer@embl.de

## Data

- 2D multi-channel widefield fluorescence microscopy data
  - Channel 1: Nuclei staining with DAPI
  - Channel 2: Autophagic activity staining tfLC3 (GFP)
  - Channel 3: Autophagic activity staining tfLC3 (RFP)

- Example input data
  - Two example image sets can be found in this repository: [data/input](data/input)

## Analysis overview

### Image analysis

Below are two screenshots that depict the output of the image analysis, which essentially segments the cells and the autophagosomes (nuclei are outlined in red, cells in blue and autophagosomes in yellow). The raw data corresponding to these two images is available [here](data/input) in this repository.

#### Positive control

Autophagy inhibition under starvation conditions, i.e. only a few autophagosomes per cell.

![](documentation/KIAA0652--CellProfiler.png)

#### Negative control

Normal autophagy under starvation conditions, i.e. several autophagosomes per cell.

![](documentation/SNX24--CellProfiler.png)

### Statistical data analysis

The key output of the image analysis is the average (per image) number of autophagosomes per cell.
TODO



TODO



## Analysis software installation

### CellProfiler installation

The screen was conducted using Cellprofiler version 2.0.11710.
For convenience we also ship a pipeline that works with CellProfiler version 2.2.0, which follows the now standard layout of CellProfiler with a separate block for configuring the input data.
We recommend working with version 2.2.0.

- Go to: https://cellprofiler.org/previous-releases
- Install version `2.2.0` (recommended) or `2.0.11710` (not recommended).

## Workflow

The screen was analysed on a high performance compute cluster (HPC). This cannot be readily reproduced as it (i) requires access to an HPC, (ii) access to all data, and (iii) specific configurations depending on the HPC.

Here, we report a workflow that can be used to analyse the two example images contained in this repository. This entails all conceptually important steps. Running the same workflow in a HPC environment may be technically challenging but does not affect the scientific aspects of this analysis.

### Image analysis with Cellprofiler

For details on how to use CellProfiler, please check here:
- https://cellprofiler.org/manuals
- https://cellprofiler.org/tutorials

1. Open CellProfiler (recommended version 2.2.0)
1. In CellProfiler, load the autophagy analysis [cellprofiler pipeline](code/cellprofiler/Autophagy--ScanR_20x--2014-03-20--cp2.2.0.cpproj)
1. Add input image data sets, e.g. [example input images](data/input).
1. Run the pipeline
1. Inspect the analysis results, e.g. [example output data](data/output)
  - For each input image data set the CellProfiler pipeline outputs:
    - One `png` image file containing the overlay of the nuclei, cell, and autophagosomes (see screenshots above)
    - One `image.csv` file containing per image measurements, i.e. a table with one row (the image) and several columns (the measurments)

For analysis of the HTM screen the CellProfiler pipeline was run on a HPC cluster on one or several plates, generating as many `image.csv` files as imaged positions. These `image.csv` files were merged into one large table for further analysis.

An example for the merged analysis results of one plate can be found in [data/output](data/output).

#### Description of the image analysis steps within the Cellprofiler pipeline

TODO

### Statistical data analysis with R

TODO

## References

- Carpenter AE, Jones TR, Lamprecht MR, Clarke C, Kang IH, Friman O, Guertin DA, Chang JH, Lindquist RA, Moffat J, Golland P, Sabatini DM (2006) CellProfiler: image analysis software for identifying and quantifying cell phenotypes. Genome Biology 7:R100. PMID: 17076895

